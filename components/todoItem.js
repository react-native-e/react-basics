import { TouchableOpacity, StyleSheet, Text, View } from "react-native";
import React from "react";
import { MaterialIcons } from "@expo/vector-icons";

export default function TodoItem({ item }) {
  return (
    <TouchableOpacity>
      <View style={styles.item}>
        <MaterialIcons name="delete" size={18} color="#333" />
        <Text style={styles.itemText}>{item.text}</Text>
      </View>
    </TouchableOpacity>
  );
}

const styles = StyleSheet.create({
  item: {
    padding: 16,
    marginTop: 16,
    borderColor: "#bbb",
    borderStyle: "dashed",
    borderRadius: 10,
    borderWidth: 1,
    flexDirection: "row",
  },

  itemText: {
    marginLeft: 10,
  },
});
