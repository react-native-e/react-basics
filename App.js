import {
  View,
  StyleSheet,
  Text,
  FlatList,
  Alert,
  TouchableWithoutFeedback,
  Keyboard,
} from "react-native";
import React, { useState } from "react";
import Header from "./components/header";
import TodoItem from "./components/todoItem";
import AddTodoItem from "./components/addTodoItem";

export default function App() {
  const submitHandler = (text) => {
    if (text.lenght > 3) {
      setTodos((prevTodos) => {
        return [{ text: text, key: Math.random().toString }, ...prevTodos];
      });
    } else {
      Alert.alert("OOPS!", "Todo must be over 2 chars long", [
        { text: "Understood", onPress: () => {} },
      ]);
    }
  };

  const [todos, setTodos] = useState([
    { text: "Buy coffe", key: "1" },
    { text: "Create an app", key: "2" },
    { text: "play on the swithc", key: "3" },
    { text: "Back to home", key: "4" },
  ]);
  return (
    <TouchableWithoutFeedback
      onPress={() => {
        Keyboard.dismiss();
      }}
    >
      <View style={styles.container}>
        <Header />
        <View style={styles.content}>
          <AddTodoItem submitHandler={submitHandler} />
          <View style={styles.list}>
            <FlatList
              data={todos}
              renderItem={({ item }) => <TodoItem item={item} />}
            />
          </View>
        </View>
        <Text style={styles.title}></Text>
      </View>
    </TouchableWithoutFeedback>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
  },

  content: {
    padding: 40,
  },
  list: {
    marginTop: 20,
  },
});

// import { useState } from "react";
// import {
//   Button,
//   FlatList,
//   ScrollView,
//   StyleSheet,
//   Text,
//   TextInput,
//   TouchableOpacity,
//   View,
// } from "react-native";

// export default function App() {
//   // const [name, setName] = useState("Hasana");
//   // const [age, setAge] = useState("40");
//   // const [person, setPerson] = useState({ name: "Oloyo", age: 40 });
//   // const clickHandler = () => {
//   //   setName("Joy");
//   //   setPerson({ name: "Habib", age: 45 });
//   // };

//   const perssHandler = (id) => {
//     console.log(id);
//   };

//   const [people, setPeople] = useState([
//     { name: "Mahbub", id: "1" },
//     { name: "Mahbub2", id: "2" },
//     { name: "Mahbub3", id: "3" },
//     { name: "Mahbub4", id: "4" },
//     { name: "Mahbub5", id: "5" },
//     { name: "Mahbub6", id: "6" },
//     { name: "Mahbub7", id: "7" },
//     { name: "Mahbub8", id: "8" },
//   ]);
//   return (
//     <View style={styles.container}>
//       <FlatList
//         numColumns={2}
//         keyExtractor={(item) => item.id}
//         data={people}
//         renderItem={({ item }) => (
//           <TouchableOpacity onPress={() => perssHandler(item.id)}>
//             <Text style={styles.item}>{item.name}</Text>
//           </TouchableOpacity>
//         )}
//       />
//     </View>

//     /*<View style={styles.container}>
//       <ScrollView>
//         {people.map((item) => (
//           <View key={item.key}>
//             <Text style={styles.item}>{item.name}</Text>
//           </View>
//         ))}
//       </ScrollView>
//         </View>*/
//     /*<View style={styles.container}>
//       <Text>Enter name</Text>
//       <TextInput
//         multiline
//         style={styles.input}
//         placeholder="e.g. Ebrahim Joy"
//         onChangeText={(value) => setName(value)}
//       />
//       <Text>Enter age</Text>
//       <TextInput
//         keyboardType="numeric"
//         style={styles.input}
//         placeholder="e.g. 45"
//         onChangeText={(value) => setAge(value)}
//       />

//       <Text>
//         My name is {name} and his age is {age}
//       </Text>
//       {/* <View style={styles.buttonContainer}>
//         <Button title="Update Name" onPress={clickHandler} />
//       </View>
//     </View>*/
//   );
// }
// const styles = StyleSheet.create({
//   container: {
//     flex: 1,
//     backgroundColor: "#fff",
//     paddingTop: 40,
//     paddingHorizontal: 20,
//   },
//   item: {
//     marginTop: 24,
//     padding: 30,
//     backgroundColor: "pink",
//     fontSize: 24,
//     marginHorizontal: 10,
//     marginTop: 24,
//   },
// });

// // const styles = StyleSheet.create({
// //   container: {
// //     flex: 1,
// //     backgroundColor: "#fff",
// //     alignItems: "center",
// //     justifyContent: "center",
// //   },

// //   buttonContainer: {
// //     marginTop: 20,
// //   },
// //   input: {
// //     borderWidth: 1,
// //     borderColor: "#777",
// //     padding: 8,
// //     margin: 10,
// //     width: 200,
// //   },
// // });
